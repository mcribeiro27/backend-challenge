# Teste Backend

## Apresentação

Documentação para utilização da Doc API - Desafio Backend.

## Autenticação

Não é preciso utilizar autenticação para fazer requisições a esta API.

## Começando

Para acessar o sistema serão necessários os seguintes programas:

- [Python 3.7.5: necessário para a execução do sistema](https://www.python.org/downloads/)

## Desenvolvimento

Para iniciar o desenvolvimento, é necessário clonar o projeto do Github em um diretório de sua preferência:
```commandline
cd "diretorio de sua preferencia"
git clone git clone https://mcribeiro27@bitbucket.org/devanddelivery/backend-challenge.git
```

## Construção

Para construir o projeto, execute os comandos abaixo dentro da pasta onde baixou o projeto:

Para ambiente Unix
```commandline
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```
Para ambiente Windows
```commandline
pip install virtualenv
virtualenv venv
venv/Scripts/activate.bat
pip install -r requirements.txt
```
O Comando irá instalar todos os módulos necessários para a execução do sistema

## Configuração

### TaskList

Está pasta representa um objeto do tipo taskList na Doc Api – Desafio Backend.

### POST Nova taskList

```url
http://127.0.0.1:5000/taskList
```
Cadastra uma nova taskList no sistema

### Body

```
{   
    "list_id": 3,
    "name": "lista de fruta"
}
```
### GET Lista taskList

```url
http://127.0.0.1:5000/taskList
```
Lista todas as taskList no sistema

### Response
```
{
  "taskList": [
    {
      "id": 3,
      "name": "lista de fruta"
    }
  ]
}
```
### GET Busca taskList

```url
http://127.0.0.1:5000/taskList/<int:list_id>
```
Busca uma taskList no sistema

### Response
```
{
    "id": 1,
    "name": "lista de fruta"
}
```
### PUT Atualiza taskList

```url
http://127.0.0.1:5000/taskList/<int:list_id>
```
Modifica/atualiza uma taskList no sistema

### Body
```
{
    "name": "lista de fruta"
}
```
### DELETE Apaga taskList

```url
http://127.0.0.1:5000/taskList/<int:list_id>
```
Deleta uma taskList no sistema

### Response
```
{
  "menssage": "taskList deleted"
}
```

### Tasks

Está pasta representa um objeto do tipo tasks na Doc Api – Desafio Backend.

### POST Nova tasks

```url
http://127.0.0.1:5000/tasks
```
Cadastra uma nova tasks no sistema

### Body

```
{
    "task_id": 1,
    "title": "Lista de compras",
    "notes": "uma anotação",
    "priority": 1,
    "activityType": "indoor",
    "status": "open",
    "taskList": 1
}
```
### GET Lista tasks

```url
http://127.0.0.1:5000/tasks
```
Lista todas as tasks no sistema

### Response
```
{
  "tasks": [
    {
      "activityType": "indoor",
      "id": 1,
      "notes": "uma anotação",
      "priority": 1,
      "remindMeOn": "Thu, 10 Sep 2020 15:03:56 GMT",
      "status": "open",
      "tags": [
        {
          "count": 1,
          "id": 1,
          "name": "fruta"
        }
      ],
      "taskList": 1,
      "title": "Lista de compra"
    }
  ]
}
```
### GET Busca tasks

```url
http://127.0.0.1:5000/tasks/<int:task_id>
```
Busca uma tasks no sistema

### Response
```
{
    "activityType": "indoor",
    "id": 1,
    "notes": "uma anotação",
    "priority": 1,
    "remindMeOn": "Thu, 10 Sep 2020 15:03:56 GMT",
    "status": "open",
    "tags": [
      {
        "count": 1,
        "id": 1,
        "name": "fruta"
      }
    ],
    "taskList": 1,
    "title": "Lista de compra"
}
```
### PUT Atualiza tasks

```url
http://127.0.0.1:5000/tasks/<int:task_id>
```
Modifica/atualiza uma tasks no sistema

### Body
```
{
    "title": "Lista de compras",
    "notes": "uma anotação",
    "priority": 1,
    "activityType": "indoor",
    "status": "open",
    "taskList": 1
}
```
### DELETE Apaga tasks

```url
http://127.0.0.1:5000/tasks/<int:task_id>
```
Deleta uma tasks no sistema

### Response
```
{
  "menssage": "tasks deleted"
}
```
### Tags

Está pasta representa um objeto do tipo tags na Doc Api – Desafio Backend.

### POST Nova tags

```url
http://127.0.0.1:5000/tags
```
Cadastra uma nova tags no sistema

### Body

```
{
    "tag_id": 1,
    "name": "frutas",
    "count": 1
}
```
### GET Lista tags

```url
http://127.0.0.1:5000/tags
```
Lista todas as tags no sistema

### Response
```
{
  "tags": [
    {
      "count": 1,
      "id": 1,
      "name": "fruta"
    },
    {
      "count": null,
      "id": 2,
      "name": "quitanda"
    }
  ]
}
```
### GET Busca tags

```url
http://127.0.0.1:5000/tags/<int:tags_id>
```
Busca uma tags no sistema

### Response
```
{
    "count": 1,
    "id": 1,
    "name": "fruta"
}
```
### PUT Atualiza tags

```url
http://127.0.0.1:5000/tags/<int:tags_id>
```
Modifica/atualiza uma tags no sistema

### Body
```
{
    "name": "frutas",
    "count": 1
}
```
### DELETE Apaga tags

```url
http://127.0.0.1:5000/tags/<int:tag_id>
```
Deleta uma tags no sistema

### Response
```
{
  "menssage": "tags deleted"
}
```

## Teste

Foi realizado com a biblioteca pytest.