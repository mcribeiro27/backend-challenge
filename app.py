import logging
from flask import Flask, jsonify
from views.list import taskList
from views.tag import tag
from views.task import task


app=Flask(__name__)
app.register_blueprint(taskList)
app.register_blueprint(tag)
app.register_blueprint(task)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

logging.basicConfig(
    filename='app.log',
    level=logging.DEBUG,
    format= "%(asctime)s [ %(levelname)s ] %(name)s\n" +
            "[ %(funcName)s ] [ %(filename)s, %(lineno)s ] %(message)s",
    datefmt="[ %d/%m/%Y %H:%M:%S]"
)

@app.before_first_request
def cria_banco():
    db.create_all()

@app.route('/')
def index():
    return jsonify({'message':'hello world'})

if __name__ == '__main__':
    from sql import db
    db.init_app(app)
    app.run(host="0.0.0.0", port=int("5000"), debug=True)