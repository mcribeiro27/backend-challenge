from sql import db

class ListModel(db.Model):
    __tablename__ = 'taskList'

    list_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    tasks = db.relationship('TaskModel', backref='task')

    def __init__(self, list_id, name):
        self.list_id = list_id
        self.name = name

    def json(self):
        return {
            'id': self.list_id,
            'name': self.name
        }

    # Busca na tabela taskList pelo id
    @classmethod
    def find_list(cls, list_id):
        lists = cls.query.filter_by(list_id=list_id).first()
        if lists:
            return lists
        return None

    # Salva na tabela taskList
    def save_list(self):
        db.session.add(self)
        db.session.commit()

    # Atualiza a tabela taskList
    def update_list(self, name):
        self.name = name

    # apaga na tabela taskList
    def delete(self):
        db.session.delete(self)
        db.session.commit()
