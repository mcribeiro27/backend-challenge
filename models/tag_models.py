from sql import db

class TagModel(db.Model):
    __tablename__ = 'tag'

    tag_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    count = db.Column(db.Integer, db.ForeignKey('task.task_id'))

    def __init__(self, tag_id, name, count):
        self.tag_id = tag_id
        self.name = name
        self.count = count

    def json(self):
        return {
            'id': self.tag_id,
            'name': self.name,
            'count': self.count
        }

    # Busca na tabela tag
    @classmethod
    def find_tag(cls, tag_id):
        tags = cls.query.filter_by(tag_id=tag_id).first()
        if tags:
            return tags
        return None

    # Salva na tabela tag
    def save_tag(self):
        db.session.add(self)
        db.session.commit()

    # Atualiza a tabela tag
    def update_tag(self, name, count):
        self.name = name
        self.count = count

    # apaga na tabela tag
    def delete(self):
        db.session.delete(self)
        db.session.commit()
