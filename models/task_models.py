import datetime
from sql import db

class TaskModel(db.Model):
    __tablename__ = 'task'
    
    task_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    notes = db.Column(db.String(150))
    priority = db.Column(db.Integer)
    remindMeOn = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    activityType = db.Column(db.String(15))
    status = db.Column(db.String(15))
    taskList = db.Column(db.Integer, db.ForeignKey('taskList.list_id'))
    tags = db.relationship('TagModel', backref='tag')

    def __init__(self, task_id, title, notes, priority, activityType, status, taskList):
        self.task_id = task_id
        self.title = title
        self.notes = notes
        self.priority = priority
        self.activityType = activityType
        self.status = status
        self.taskList = taskList


    def json(self):
        return {
            'id': self.task_id,
            'title': self.title,
            'notes': self.notes,
            'priority': self.priority,
            'remindMeOn': self.remindMeOn,
            'activityType': self.activityType,
            'status': self.status,
            'taskList': self.taskList,
            'tags': [tag.json() for tag in self.tags]
        }

    # Busca na tabela task
    @classmethod
    def find_task(cls, task_id):
        tasks = cls.query.filter_by(task_id=task_id).first()
        if tasks:
            return tasks
        return None

    # Salva na tabela task
    def save_task(self):
        db.session.add(self)
        db.session.commit()

    # Atualiza a tabela task
    def update_task(self, title, notes, priority, activityType, status, taskList):
        self.title = title
        self.notes = notes
        self.priority = priority
        self.activityType = activityType
        self.status = status
        self.taskList = taskList

    # apaga na tabela task
    def delete(self):
        db.session.delete(self)
        db.session.commit()
