import os
import tempfile
import pytest
from app import app, cria_banco
from flask_sqlalchemy import SQLAlchemy
import json 
import requests
 
@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    sqlalchemy = SQLAlchemy()
    sqlalchemy.init_app(app)

    yield client

def test_list_post(client):
    url = "http://127.0.0.1:5000/taskList"
    headers = {
            'Content-Type': 'application/json'
        }
    payload = { "list_id": 1, "name": "lista de frutas"}

    rv = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    print(rv)
    assert rv.status_code == 201


def test_list_get(client):
    url = "http://127.0.0.1:5000/taskList"
    headers = {
            'Content-Type': 'application/json'
        }
    rv = requests.request("GET", url, headers=headers)
    
    assert rv.status_code == 200

def test_list_get_unico(client):
    url = "http://127.0.0.1:5000/taskList/1"
    headers = {
            'Content-Type': 'application/json'
        }

    rv = requests.request("GET", url, headers=headers)
    assert rv.status_code == 200

def test_list_put(client):
    url = "http://127.0.0.1:5000/taskList/1"
    headers = {
            'Content-Type': 'application/json'
        }
    payload = {"name": "lista de fruta"}

    rv = requests.request("PUT", url, headers=headers, data=json.dumps(payload))
    print(rv)
    assert rv.status_code == 200

def test_list_delete(client):
    url = "http://127.0.0.1:5000/taskList/1"
    headers = {
            'Content-Type': 'application/json'
        }

    rv = requests.request("DELETE", url, headers=headers)
    assert rv.status_code == 200