import os
import tempfile
import pytest
from app import app, cria_banco
from flask_sqlalchemy import SQLAlchemy
import json 
import requests
 
@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    sqlalchemy = SQLAlchemy()
    sqlalchemy.init_app(app)

    yield client

def test_task_post(client):
    url = "http://127.0.0.1:5000/tasks"
    headers = {
            'Content-Type': 'application/json'
        }
    payload = { 
        "task_id": 1,
        "title": "Teste de task",
        "notes": "teste de notas",
        "priority": 1,
        "activityType": "indoor",
        "status": "open",
        "taskList": 1
    }

    rv = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    print(rv)
    assert rv.status_code == 201

def test_task_get(client):
    url = "http://127.0.0.1:5000/tasks"
    headers = {
            'Content-Type': 'application/json'
        }
    rv = requests.request("GET", url, headers=headers)
    
    assert rv.status_code == 200

def test_task_get_unico(client):
    url = "http://127.0.0.1:5000/tasks/1"
    headers = {
            'Content-Type': 'application/json'
        }

    rv = requests.request("GET", url, headers=headers)
    assert rv.status_code == 200

def test_task_put(client):
    url = "http://127.0.0.1:5000/tasks/1"
    headers = {
            'Content-Type': 'application/json'
        }
    payload = {
        "title": "Teste de task",
        "notes": "teste de nota",
        "priority": 1,
        "activityType": "indoor",
        "status": "open",
        "taskList": 1
    }

    rv = requests.request("PUT", url, headers=headers, data=json.dumps(payload))
    print(rv)
    assert rv.status_code == 200

def test_task_delete(client):
    url = "http://127.0.0.1:5000/tasks/1"
    headers = {
            'Content-Type': 'application/json'
        }

    rv = requests.request("DELETE", url, headers=headers)
    assert rv.status_code == 200