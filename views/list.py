from flask import Blueprint, request
from models.list_models import ListModel

taskList = Blueprint('tasklist_routes', __name__, url_prefix= '/taskList')

# recupera todos os taskList
@taskList.route('')
def get_all():
    return {'taskList': [taskList.json() for taskList in ListModel.query.all()]}, 200

# cadastra as taskList
@taskList.route('', methods=['POST'])
def post():
    query = request.json
    taskList = ListModel(**query)

    if ListModel.find_list(query['list_id']):
        return {'message': "taskList '{}' already exists.".format(query['list_id'])}, 400
    try:
        taskList.save_list()
        return {'message': 'taskList successfully created'}, 201
    except:
        return {'message': 'Internal error'}, 500
    return taskList.json()

# recupera uma taskList
@taskList.route('/<int:list_id>')
def get(list_id):
    taskList = ListModel.find_list(list_id)
    if taskList:
        return taskList.json(), 200
    return {'message': 'taskList not found'}, 400

# atualiza as taskList
@taskList.route('/<int:list_id>', methods=['PUT'])
def put(list_id):
    query = request.json
    taskList_encontrado = ListModel.find_list(list_id)

    # caso exista uma taskList, atualiza
    if taskList_encontrado:
        taskList_encontrado.update_list(**query)
        try:
            taskList_encontrado.save_list()
        except:
            return {'message':'internal error'}, 500
        return taskList_encontrado.json(), 200

    # caso não exista, cria-se uma nova taskList
    taskList = ListModel(list_id, **query)
    try:
        taskList.save_list()
    except:
        return {'message': 'internal error'}, 500
    return taskList.json(), 201

# Deleta uma taskList
@taskList.route('/<int:list_id>', methods=['DELETE'])
def delete(list_id):
    taskList = ListModel.find_list(list_id)
    if taskList:
        taskList.delete()
        return {'message': 'taskList deleted'}, 200
    return {'message': 'taskList not found'}, 400