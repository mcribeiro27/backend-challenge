from flask import Blueprint, request
from models.tag_models import TagModel

tag = Blueprint('tag_routes', __name__, url_prefix='/tags')

# recupera todas tags
@tag.route('')
def get_all():
    return {'tags': [tag.json() for tag in TagModel.query.all()]}, 200

# cadastra as tags
@tag.route('', methods=['POST'])
def post():
    query = request.json
    tag = TagModel(**query)

    if TagModel.find_tag(query['tag_id']):
        return {'message': "tag '{}' already exists.".format(query['tag_id'])}, 400
    try:
        tag.save_tag()
        return {'message': 'tag successfully created'}, 201
    except:
        return {'message': 'Internal error'}, 500
    return tag.json()

# recupera uma tag
@tag.route('/<int:tag_id>')
def get(tag_id):
    tag = TagModel.find_tag(tag_id)
    if tag:
        return tag.json(), 200
    return {'message': 'tag not found'}, 400

# atualiza as tag
@tag.route('/<int:tag_id>', methods=['PUT'])
def put(tag_id):
    query = request.json
    tag_encontrado = TagModel.find_tag(tag_id)

    # caso exista uma tag, atualiza
    if tag_encontrado:
        tag_encontrado.update_tag(**query)
        try:
            tag_encontrado.save_tag()
        except:
            return {'message':'internal error'}, 500
        return tag_encontrado.json(), 200

    # caso não exista, cria-se uma nova tag
    tag = TagModel(tag_id, **query)
    try:
        tag.save_tag()
    except:
        return {'message': 'internal error'}, 500
    return tag.json(), 201

# Deleta uma tag
@tag.route('/<int:tag_id>', methods=['DELETE'])
def delete(tag_id):
    tag = TagModel.find_tag(tag_id)
    if tag:
        tag.delete()
        return {'message': 'tag deleted'}, 200
    return {'message': 'tag not found'}, 400
