from flask import Blueprint, request
from models.task_models import TaskModel

task = Blueprint('task_routes', __name__, url_prefix='/tasks')

# recupera todas tasks
@task.route('')
def get_all():
    return {'tasks': [task.json() for task in TaskModel.query.all()]}, 200

# cadastra as tasks
@task.route('', methods=['POST'])
def post():
    query = request.json
    task = TaskModel(**query)

    if TaskModel.find_task(query['task_id']):
        return {'message': "task '{}' already exists.".format(query['task_id'])}, 400
    try:
        task.save_task()
        return {'message': 'task successfully created'}, 201
    except:
        return {'message': 'Internal error'}, 500
    return task.json()

# recupera uma task
@task.route('/<int:task_id>')
def get(task_id):
    task = TaskModel.find_task(task_id)
    if task:
        return task.json(), 200
    return {'message': 'task not found'}, 400

# atualiza as task
@task.route('/<int:task_id>', methods=['PUT'])
def put(task_id):
    query = request.json
    task_encontrado = TaskModel.find_task(task_id)

    # caso exista uma task, atualiza
    if task_encontrado:
        task_encontrado.update_task(**query)
        try:
            task_encontrado.save_task()
        except:
            return {'message':'internal error'}, 500
        return task_encontrado.json(), 200

    # caso não exista, cria-se uma nova task
    task = TaskModel(task_id, **query)
    try:
        task.save_task()
    except:
        return {'message': 'internal error'}, 500
    return task.json(), 201

# Deleta uma task
@task.route('/<int:task_id>', methods=['DELETE'])
def delete(task_id):
    task = TaskModel.find_task(task_id)
    if task:
        task.delete()
        return {'message': 'task deleted'}, 200
    return {'message': 'task not found'}, 400
